use colored::Colorize;
use crate::Token;

pub enum LmcResult<T> {
    Ok(T),
    Err(LmcError),
}

pub enum LmcError {
    ReadError(String),
    TokenError(usize, usize),
    ParseError,
    TypeError(String, String, Token)
}

impl LmcError {
    pub fn gen(self, src: &str) -> String {
        match self {
            LmcError::ReadError(path) => format!("Could not read source file `{}`", path.blue()),
            LmcError::TokenError(row, length) => format!(
                "Too many tokens on line `{}`: expected maximum of `{}`, found `{}`\n{}{} {}",
                row.to_string().blue(),
                "3".blue(),
                length.to_string().blue(),
                row.to_string().dimmed(),
                "|".dimmed(),
                src.split('\n').collect::<Vec<&str>>()[row].red()
            ),
            LmcError::ParseError => format!("Found unexpected newline. This is most likely a bug in the emulator."),
            LmcError::TypeError(expected, found, token) => format!(
                "Wrong type found at [{}:{}]: expected `{}`, found `{}`",
                token.row.to_string().blue(),
                token.column.to_string().blue(),
                expected.blue(),
                found.blue()
            )
        }
    }
}

#[macro_export]
macro_rules! handle {
    ($error:expr, $src:expr) => {
        match $error {
            Ok(data) => data,
            Err(err) => {
                let message = err.gen($src);
                println!("{} {}", "ERR".red(), message);
                return;
            }
        }
    };
}
