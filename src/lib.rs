use std::collections::HashMap;
use std::fs;

pub mod err;
use err::LmcError;
use err::LmcResult;
use LmcResult::*;

#[derive(Debug, PartialEq, Clone)]
pub enum TokenType {
    Number(isize),
    String(String),
    Newline,
}

#[derive(Debug, Clone)]
pub struct Token {
    tt: TokenType,
    row: usize,
    column: usize,
}

#[derive(Debug)]
pub struct Action {
    operator: String,
    operand: TokenType,
    label: Option<String>
}

fn gen_token(text: String, row: usize, column: usize) -> Token {
    Token {
        tt: text
            .parse::<isize>()
            .map(TokenType::Number)
            .unwrap_or(TokenType::String(text)),
        row,
        column,
    }
}

pub fn read_source(path: &str) -> LmcResult<String> {
    fs::read_to_string(path)
        .map(Ok)
        .unwrap_or_else(|_| Err(LmcError::ReadError(path.to_string())))
}

pub fn lex(text: String) -> LmcResult<Vec<Token>> {
    let mut result = vec![];
    let mut row = 0;
    let mut column = 0;
    let mut in_comment = false;
    let mut builder = String::new();

    for chr in text.chars() {
        if chr == '\n' {
            row += 1;
            column = 0;
            in_comment = false;
            if !builder.is_empty() {
                result.push(gen_token(builder.clone(), row, column));
                builder.clear();
            }
            let last = result.pop().unwrap();
            result.push(last.clone());
            if last.tt != TokenType::Newline {
                result.push(Token {
                    tt: TokenType::Newline,
                    row,
                    column
                });
            }
        } else if chr == '#' {
            in_comment = true;
        } else if in_comment {
            continue;
        } else if chr.is_whitespace() {
            if !builder.is_empty() {
                result.push(gen_token(builder.clone(), row, column));
                builder.clear();
            }
        } else {
            builder.push(chr);
        }
        column += 1;
    }

    Ok(result)
}

fn gen_action(tokens: Vec<Token>) -> LmcResult<Action> {
    if tokens.len() == 1 {
        match &tokens[0].tt {
            TokenType::String(operator) => Ok(Action {
                operator: operator.clone(),
                operand: TokenType::Number(0),
                label: None
            }),
            TokenType::Number(_) => Err(LmcError::TypeError(
                    "str".to_string(), 
                    "int".to_string(), 
                    tokens[0].clone()
            )),
            TokenType::Newline => Err(LmcError::ParseError)
        }
    } else if tokens.len() == 3 {
        match (&tokens[0].tt, &tokens[1].tt) {
            (TokenType::String(label), TokenType::String(operator)) => {
                Ok(Action {
                    operator: operator.clone(),
                    operand: tokens[2].tt.clone(),
                    label: Some(label.clone())
                })
            },
            _ => panic!("Wrong types")
        }
    } else {
        if let TokenType::String(string) = &tokens[0].tt {
            if ["hlt", "add", "sub", "sta", "lda", "bra", "brp", "brz", "inp", "out"].contains(&string.to_lowercase().as_str()) {
                Ok(Action {
                    operator: string.clone(),
                    operand: tokens[1].tt.clone(),
                    label: None
                })
            } else {
                if let TokenType::String(operator) = &tokens[1].tt {
                    Ok(Action {
                        operator: operator.clone(),
                        operand: TokenType::Number(0),
                        label: Some(string.clone())
                    })
                } else {
                    Err(LmcError::TypeError("str".to_string(), "int".to_string(), tokens[1].clone()))
                }
            }
        } else {
            Err(LmcError::TypeError("str".to_string(), "int".to_string(), tokens[0].clone()))
        }
    }
}

pub fn parse(tokens: Vec<Token>) -> LmcResult<Vec<Action>> {
    let mut result = vec![];
    let mut builder: Vec<Token> = vec![];

    for token in tokens {
        if token.tt == TokenType::Newline {
            if builder.len() > 3 {
                return Err(LmcError::TokenError(
                    builder[0].row,
                    builder.len()
                ))
            } else {
                result.push(match gen_action(builder.clone()) {
                    Ok(action) => action,
                    Err(e) => return Err(e)
                });
            }
            builder.clear();
        } else {
            builder.push(token);
        }
    }

    Ok(result)
}

fn opcode(operator: u8, operand: TokenType, labels: &HashMap<String, usize>) -> u16 {
    let operand = match operand {
        TokenType::Number(num) => num as usize,
        TokenType::String(label) => *labels.get(&label).unwrap(),
        TokenType::Newline => panic!("Very bad")
    };
    ((operator as usize*100) + operand) as u16
}

pub fn compile(actions: Vec<Action>) -> Vec<u16> {

    let mut labels = HashMap::new();
    for (i, action) in actions.iter().enumerate() {
        if let Some(label) = &action.label {
            labels.insert(label.clone(), i);
        }
    }

    let mut result = vec![];
    for action in actions {
        result.push(match action.operator.to_lowercase().as_str() {
            "hlt" => 0,
            "add" => opcode(1, action.operand, &labels),
            "sub" => opcode(2, action.operand, &labels),
            "sta" => opcode(3, action.operand, &labels),
            "lda" => opcode(5, action.operand, &labels),
            "bra" => opcode(6, action.operand, &labels),
            "brz" => opcode(7, action.operand, &labels),
            "brp" => opcode(8, action.operand, &labels),
            "inp" => 901,
            "out" => 902,
            "dat" => opcode(0, action.operand, &labels),
            _ => panic!("Unknown operator: {}", action.operator)
        });
    }
    result
}
