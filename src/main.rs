use colored::Colorize;
use lmc::err::LmcResult::*;
use std::env;

fn main() {
    let text = lmc::handle!(lmc::read_source("source.lmc"), "");
    let tokens = lmc::handle!(lmc::lex(text.clone()), &text);
    println!("{tokens:#?}");
    let actions = lmc::handle!(lmc::parse(tokens), &text);
    let binary = lmc::compile(actions);
    println!("{binary:#?}");
}
